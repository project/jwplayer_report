CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The JWPlayer Report module help us to get analytical data of videos based on media ids.
Using this you can download all the analytical data in the report format. Also you can
see this data in list format if you don't want to download.
eg. Data for past six month OR by giving custom date range.

* For a full description of the module, visit the project page:
   https://drupal.org/project/jwplayer_report

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/jwplayer_report

REQUIREMENTS
------------
It require account on https://www.jwplayer.com which will provide API keys for configuration.

INSTALLATION
------------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Steps:
1) Install JWPlayer Report mdoule

CONFIGURATION
-------------

 - Once you have the JWPlayer Report module installed, configure JWPlayer API Keys (https://www.jwplayer.com/)
by admin/config->System->JWPlayer Settings (admin/config/system/jwplayer_report/settings)


MAINTAINERS
-----------

Current maintainers:
* Priyanka Attarde (priyanka.attarde) - https://www.drupal.org/u/priyankaattarde
